# Flipscreen 2D platform games

## Part 1: Basic Camera Movement

### Overview
If you only need the full solution/template or are here from the 'Create Metroid in 15 minutes' and do not wish to know how it works visit the Flipscren Template project:
https://gitlab.com/chucklepie-productions/tutorials/

Tutorial youtube videos at, rather long so maybe watch the one below instead first:
https://www.youtube.com/playlist?list=PLtc9v8

'Create Metroid in 15 minutes' using this template found here: https://www.youtube.com/watch?v=Cnx3ts86qDg


This tutorial will hopefully guide you towards creating a flip-screen platform game. It focuses purely on how to create flipscreens, i.e. the camera, and not using Godot or creating games, but by the end of these 4 tutorials you should be able to:

1. Create a flip-screen 2D platformer camera. Hurrah
2. Have a basic kinematic body moving around the map to interact with the camera
3. Add a HUD, i.e. how to create multiple viewports (split the screen in two: map and HUD)

These will focus on a fixed size, non-scrolling camera/game based on tilemaps. There is an assumed level of Godot knowledge, so anything you are unsure of, you will need to do some background research.

Disclaimer: All the code you see is kept as simple and minimalistic as possible and put into scenes and folders to achieve the best tutorial experience, not necessarily a game. Please refactor to your own industrial standards and requirements.

We will be recreating an old 8 bit game called Cybernoid, created by the legendary Raphael Cecco. At the end of this tutorial we will be navigating between each room exit, but using the cursors. 

If you want to see the original game in action for reference (sorry, I gave up trying to figure out Gitlab Markdown language):
https://www.youtube.com/watch?v=qJ2JJJGdIck

Finally, note that this is not a step by step guide taking you from nothing and building up. It is more a guided tour of the final result with the aim that you will interact with the project as you read to gain an understanding, or even try to recreate the game from scratch using the project as a reference.

There is also a video to accompany this tutorial and part 1 is available here:
https://www.youtube.com/playlist?list=PLtc9v8wsy_BY8l7ViNJamL6ao1SKABM5T

It's about 40 minutes long, I tend to over explain things :)

### Getting Started
Download the project part 1 by simply selecting the download icon above and downloading the zip, then uncompress and open within Godot. If you're feeling adventurous then use the Clone option and download everything via git.

![image.png](README_Images/image.png)

This project was saved with Godot 3.3. but should work with any 3.x version (3.2.3 if you haven't upgraded yet).

Open the Godot project, run (F5) and use the cursors to navigate the rooms by following the exits. This is the basis for a flip-screen game. When done close the scene file 'Game' that probably opened by default.

When trying to figure things out, the important thing to remember is when looking at the node inspector, project settings, etc is simply just look for the 'revert' symbol and you will see what was changed from the default, i.e. everything else is the default, e.g.

![image-2.png](README_Images/image-2.png)


### Project Setup
There are many ways to create folder structures. I prefer to create folders for specific domain areas, e.g. levels, entities, etc and keep scene files together with code rather than creating functional areas such as code/scenes/etc. There are two exceptions:

1. I have an asset folder containing all the external data, such as graphics, sound
2. Imported libraries or areas of specific interest (e.g. miniboss) I keep everything in one folder

Look at the FileSystem

![image-5.png](README_Images/image-5.png)

In the assets/tilemap folder there are two bitmaps, these are the tiles used by the tutorial (double click to view) to create the tilemap. I have also included Super Metroid as a bonus if you want to try that out later.

In the levels folder open the file cybernoid.tscn. This is the game map. As above, I have included metroid. Note the additional scene files are there to simply remove the solid black tile in case a new background is required. They are just samples, we will only be using cybernoid.tscn. Scroll around, zoom in and out and be comfortable with the map.

The tilemap (cybernoid.tscn) and graphics contain no code they are simply a tilemap node and a sprite sheet. Rather than creating the tileset manually then creating the maps using the Godot editor, these were created by tools I created many years ago that can be found here, along with more samples, if you're interested:

1. https://gitlab.com/chucklepie-productions/game-utilities : create Godot tilemap scenes from bitmaps and CSV (see item 2)
2. http://retrospec.sgn.net/game/tiler : create spritesheets and map CSV from bitmap game maps. Ignore any warnings, the site is safe, honest :)

All graphics were reimported using the '2D Pixel' Preset. Select the image(s), click on 'Import' pane then click Preset button. You need to do this for your 2D pixel games otherwise you will get blur. You can set this as the default importer for the project in the same dropdown.

![image-1.png](README_Images/image-1.png)

If you can figure out how to use the tools I created you can get more maps for conversion from here to save you hand drawing your own:

https://maps.speccy.cz


### Tilemaps
If you're not familiar with tilemaps then watch a video :), but I'll summarise the changes from a default new tilemap node:
1. Open the scene levels/cybernoid.tscn, this is simpy a tilemap node
2. In the Inspector cell size is set to 32x32, this is because that is how big each tile is in pixels :)
3. Click on 'Tile Set' in the inspector, then click on the first image you see. This will load up the graphics and show you each tile
4. Go back to the tileset by clicking the '<' (or click on the scene 'TileMap_FG1') and have a draw if you like to update the map

Moving the mouse will shown each cell location and tile number on the bottom left of the screen.


![image-8.png](README_Images/image-8.png)

Turn on grid snap (or click the three dots/configure snap setting grid set to be 512x320 if creating a new project). Grid snap snaps nodes to the grid in the editor, but we are using it simply to see our screens. When we add nodes to the map and move them around we'll need to turn off grid snap. The other way is to click the View button on the same toolbar and select 'Always view grid'. I'd probably do this anyway :)

![image-3.png](README_Images/image-3.png)

The large grid/snap will let you visualise each screen better, it does not affect the game. The yellow colour was set via the 'Editor/Editor Settings...' menu item. Good luck in trying to find it yourself ;-)

![image-4.png](README_Images/image-4.png)

That is all you need to know about the tilemap, the next tutorial we'll be doing more.

Remember, look for the 'revert' icon and you'll easily see all the properties mentioned in this document.

### Project Settings
These are the project settings that were updated to aid our flipscreen camera. This is detailed just to give a basic understanding of settings needed for a 2D pixel game. From the main menu select Project/Settings.

#### Related to flipscreen
- **General/Display/Window**: set Width/Height to be 512x320 to match our room size. If you want to see something bigger, go to the 'test width' and make those a multiple, e.g. 1024x720. Test width is how the screen will be sized when ran
- **General/Rendering/2d**: use gpu pixel snap enabled. This is required for almost every pixel game
   - Note: prior to Godot 3.3 (you may be on 3.2.3) this setting is under General/Rendering/Quality and called 'use pixel snap', you will need to do this manually right now if not using 3.3
- **General/Display/Window**: Stretch set to viewport mode and keep aspect. This ensure resizing the screen does not stretch pixels. For the difference between viewport and 2D mode refer to the Godot docs https://docs.godotengine.org/en/stable/tutorials/viewports/multiple_resolutions.html

#### Not related to flipscreen but may be useful
- **General/Memory/Memory/Limits**: message queue increased to 102400, this is because (possibly fixed in 3.3) having large number of tiles in a tileset crashes Godot due to how it manages the UI, my other game has a few hundred and dies horribly in Godot as it tries to create a few thousand UI widgets without this setting
- **General/Network**: the max chars per second was increased to allow more text to be output to the debug window, i.e. the print() method
- **General/Rendering/Quality**: Dynamic font oversampling disabled due to Godot causing problems with certain fonts
- **General/Run**: the start scene was set for playing the project (when F5 is pressed)
- **General/Display/Window**: the resolution set to match the game

### Camera Overview
Finally, we're starting the tutorial.

We want our camera to show each room as shown above and flip as shown in the youtube videos, rather than scroll with the player. The basics are:

1. Create a camera as a parent of the game scene, not the player or some other node
2. Caculate our screen size
3. Update the camera position manually by our screen size and we will get a screen flip

Why does this work?

With a camera that follows the player you put the camera inside the player node, in other words when the player 'position' property changes, so does the camera 'position' property. For flipscreen the camera simply sits in the scene tree and does not move unless we move it

When calculating our screen:
  - We know each tile/cell is 32x32 pixels
  - We know each room is 512x320 pixels
  - therefore each room is 16x10 cells in size

We also know the top left of our map is position 0,0 and x/y increase as you go right and down (just like all nodes in Godot). The camera works in pixels, so, to flip screens we need to adjust camera position by 512 pixels (16 tiles) when moving left/right or 320 pixels (10 cells) when moving up/down. That is all there is to it, because the camera is not positioned relative to any node. Nothing could be simpler...

Don't worry, we'll look at the camera soon.

### Room Overview
We need to give each room an ID, i.e. a room number, to aid navigation and gameplay. The simplest way is the top left room is 0, the next is 1, etc.

In order to go to the next row we need to know how many screens wide the map is, if you look at the map it is almost a grid of 6x4 rooms. So we could just wrap at this, i.e. the second row from the top starts with room 6, the next row 12, etc. The only problem with this is if we expand the map we are a bit stuck for sequential IDs.

It doesn't really matter how wide we say it is as long as we calculate the row numbers properly. As below I have set it at 10 as simply a nice round number. Rooms 6,7,8,9, etc don't exist but it doesn't matter, as we'll see shortly in the navigation code.

![image-6.png](README_Images/image-6.png)

### Room Calculations
We're doing some basic maths now, feel free to skip.

Assume a constant MAP_WIDTH_SCREENS = 10, to represent this map width just mentioned.

Ignoring the ends of the map, to move left or right we simply add or subtract one, e.g. room 5 has rooms 4 and 6 either side.

To move up or down the formula is simply to add/subtract this map width value:

```
#in room 5, to move down it would be 5+10
new_room_id = current_room_id + (MAP_WIDTH_SCREENS * vertical_direction)
```

Where vertical_direction is 1 or -1 for down or up, i.e. going down from 12 would take us to 22, but up would be 2.

In the game we will probably work in pixels, so assuming we have a body called 'player', here is how we can calcuate some things relative to the player:
```
const MAP_WIDTH_SCREENS:=10     #number of screens in our game grid
const CELL_SIZE:=32             #pixel size of each tile/cell
const MAP_SCREEN_SIZE_CELLS:=Vector2(16,10) #the size of each screen in tiles
const MAP_SCREEN_SIZE_PIXELS:=MAP_SCREEN_SIZE_CELLS*CELL_SIZE #512x320
```
The key value is our screen size (512x320) which we calculate via MAP_SCREN_SIZE_PIXELS and use to position the camera.

We will also want to know how to find any given room number from a pixel position, e.g. what room the player is in. The simplest way is first find the map grid reference (e.g. room 23 is 4 across and 3 down):
```
var screen_x = int(player.global_position.x / MAP_SCREEN_SIZE_PIXELS.x)
var screen_y = int(player.global_position.y / MAP_SCREEN_SIZE_PIXELS.y)
```
i.e. we simply get the whole number of the pixel position divided by each room size, e.g. if player is at location 2000,800 then the screen would be 3,2 (i.e. room 23) as 2000/512 is 3, 800/32 is 2

Once we know the screen position (i.e. 3,2), to get the room id we take these screen grid co-ordinates and find the room:
```
var room_id = (screen_y * MAP_WIDTH_SCREENS) + screen_x
```
i.e. we find the row then add the offset from x co-ordinate

### Map definition and navigation data
We need to know about each room (e.g. we may want to spawn enemies differently) and we need to know which rooms we can move into (e.g. in room 12 - see image above - you can go up or left only, or we may only want one-way navigation so in room 12 you can only go left)
Open the scene scenes/map.tscn

This represents our map (not the game), TileMap_FG1 is a scene instance of cybernoid.tscn

Open the code associated with map.tscn and you will see all the variables we defined in the previous section. 

The constant ROOM_DATA is a basic dictionary where each key is the room number. Each item has one element, an array, containing the rooms we can get to from each screen, i.e. from room 13 we can go down to room 23 and right to room 14.

Normally this will contain more room data, e.g. information on enemies, etc. and probably loaded from a file.

Press F6 to run this specific scene and go to the Output tab, you will see our debug data showing all is as expected.

Observe the function get_roomid_from_global()
This converts global pixel coordinates to a room id, which is simply the implementation of the code detailed in the previous section.

Now observe the method get_valid_room_in_direction_from_position() and get_valid_room_in_direction_from_room()

This takes in a position (or room id), e.g. current player location and a direction (up,down,left,right) and returns whether it's valid to move to a screen in that direction. 

The method simply gets the room id from the position as above then checks the ROOM_DATA to see if the room has an exit in that direction. It then returns the new room, or invalid return value.

This should be enough to get our camera working

Ingore everything else for now.

### Navigation
We will position the camera at the top left of every room we see in our tilemap.

Observe the method get_room_topleft_global() in the code and our screenshot above showing room numbers.

To get the top position of the room we simply divide the room id by the number of screens per row (i.e. 10 in our example) and get the integer whole, so 23 would be 2.

To get the left position of the room we get the remainder from dividing the room number by the same value 10 (i.e. 23 would be 3). This is called modulus, the '%' operator.

We then multiply these numbers by the screen size in pixels.

Press F6 and check the debug Output window for verification.

Note: if we wanted to get this top left location from the players position rather than a room number, all we need to do is create a function that does two things:

1. Calls get_roomid_from_global() passing in the player id
2. Call get_room_topleft_global() passing the return from (1)
3. There is no need for validation as the methods all self-validate

This is shown in the method get_room_top_left_global_from_position()

### Camera
We are now ready for the camera and a flipscreen :)

Open up camera.tscn

This is simply a camera. We are saving it as a separate scene because in the next tutorial we will expand on it, and it's always best to use scene instancing as it makes things more portable.

Essentially, the camera represents a view of the screen for whatever layer is being shown. When no camera is in a game the current viewport (i.e. the blue bounding box you see in the IDE) is the camera.

Select the Camera2D node, Godot will show the camera as the blue rectangle and the cross-hair the origin.

Ofbserve the following inspector properties that we set in order to create our flip screen camera, try adjusting to see the effect, but remember to put them back:

| Property | Value | Details |
| ---      |  ------  |----------|
| Anchor Mode   | Fixed TopLeft   | Where the origin is, we want top left to match our code   |
| Current   | True   | Whether the camera is the active one in the game   |
| Transform Position   | 0,0   | This is where the camera will point at   |
This is all that is needed as the camera takes its size from the viewport it belongs to. You can use the zoom property (not scale) to scale the map

So all we have to do is alter the position property and we have a flip screen.

We do this via the method set_position() that can be found in the code attached to the camera. This really doesn't do much, but it's better to call methods than directly set properties sometimes. We don't validate location as we expect the map or room system to do that, however we can make setting the position, the zoom level and performing any validation we want.

### Flip screen
Open up game.tscn

This simply has the camera and the map scenes instanced. And recall, the Map scene contains the tilemap instance.

As mentioned, creating scenes that are made up of instances make our scene tree easier to manage (as they are smaller given the sub-scenes are hidden) and easier to reuse our components (scenes), and therefore test.

Open up the code.

1. We have a variable 'current_room' that contains the room we are now in. Maybe it should be in the map code, that might be better :)

2. The input event simply checks for cursor keys and processes it

3. _manage_direction() is given a direction (up,down,left,right) and simply makes use of the code we've already written to get the room in the direction we want, or an invalid room if there is a problem and sets our 'current_room' if applicable

4. _change_room() gets the global pixel location of the 'current_room' and asks the camera to reposition itself.

There isn't much code because as mentioned it's handled by the map code.

So run the game and follow the screen gaps using the cursors.


### Next
Next tutorial will add a basic kinematic body (i.e. the player) and allow navigation. This will involve adding collision shapes and static bodies to the camera to control movement and trigger signals.

After that, if there is interested, we can put in a HUD as shown below above the screen, given viewport containers are a tricky thing to do.

![image-7.png](README_Images/image-7.png)

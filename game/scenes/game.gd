extends Node2D

#the final solution can be found at the template page: https://gitlab.com/chucklepie-productions/flipscreen-template
#Also a 'create Metroid in 15 minutes' can be found here: https://www.youtube.com/watch?v=Cnx3ts86qDg

onready var camera_controller:Camera2D=$Camera2D
onready var map_controller:Node2D=$Map

onready var current_room=map_controller.START_ROOM

func _ready() -> void:
	#set us at the start room
	_change_room()

func _input(event: InputEvent) -> void:
	#get the key that was presed
	var direction=-1
	if Input.is_action_just_pressed("ui_down"):
		direction=map_controller.ROOM_DIRECTION.DOWN
	if Input.is_action_just_pressed("ui_up"):
		direction=map_controller.ROOM_DIRECTION.UP
	if Input.is_action_just_pressed("ui_left"):
		direction=map_controller.ROOM_DIRECTION.LEFT
	if Input.is_action_just_pressed("ui_right"):
		direction=map_controller.ROOM_DIRECTION.RIGHT

	_manage_direction(direction)
	
func _manage_direction(direction:=-1):
	#get the new room, the methods will handle invalid values just fine
	var newroom=map_controller.get_valid_room_in_direction_from_room(current_room,direction)
	if newroom==-1:
		return
		
	#we have a new room, simply set it, mark input as handled then 
	#ask the camera nicely to change room
	current_room=newroom
	get_tree().set_input_as_handled()
	_change_room()

func _change_room():
	#take the current room and set the camera to this
	var newpos=map_controller.get_room_topleft_global(current_room)
	
	if newpos==map_controller.INVALID_ROOM:
		return
		
	camera_controller.set_position(newpos)
